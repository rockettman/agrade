import sys
import pandas as pd
import numpy as np


DATA_FILENAME = sys.argv[1]
TOLERANCE = float(sys.argv[2])
OUT_FILENAME = sys.argv[3]

COLUMNS = ["northing", "easting", "elevation", "type"]

field = pd.read_csv(DATA_FILENAME,
                    header=None,
                    names=COLUMNS)
field.sort_values(by=["easting", "northing"], inplace=True)
field.drop_duplicates(subset=COLUMNS, inplace=True)
field.reset_index(drop=True, inplace=True)

array_counter = 0
in_the_middle = False


def assign_to_array(pile):
    global in_the_middle
    global array_counter
    if pile["type"].endswith("END"):
        if not in_the_middle:
            in_the_middle = True
            array_counter += 1
        else:
            in_the_middle = False
    return array_counter


field["array"] = field.apply(assign_to_array, axis=1)


def get_straight_grade_function(array):
    p1 = array.iloc[0]
    p2 = array.iloc[-1]

    y1 = p1["northing"]
    z1 = p1["elevation"]

    y2 = p2["northing"]
    z2 = p2["elevation"]

    slope = (z2 - z1) / (y2 - y1)

    def line(y):
        return slope * (y - y1) + z1

    return line


def calc_straight_grade_elevation(array):
    straight_grade_func = get_straight_grade_function(array)
    array["SGE"] = array["northing"].apply(straight_grade_func)
    return array


def calc_sge_offset(pile):
    return pile["elevation"] - pile["SGE"]


def straight_grade_check(array):
    max_positive_offset = array["sge_offset"].max()
    max_negative_offset = array["sge_offset"].min()
    array["sge_check"] = max_positive_offset - max_negative_offset < TOLERANCE
    return array


def get_best_fit_line(array):
    return np.polynomial.Polynomial.fit(
        array["northing"], array["elevation"], 1
    )


def calc_best_fit_elevation(array):
    best_fit = get_best_fit_line(array)
    array["BFE"] = array["northing"].apply(best_fit)
    return array


def calc_best_fit_offset(pile):
    return pile["elevation"] - pile["BFE"]


def best_fit_check(array):
    max_positive_offset = array["bfe_offset"].max()
    max_negative_offset = array["bfe_offset"].min()
    array["bfe_check"] = max_positive_offset - max_negative_offset < TOLERANCE
    return array


output = field.groupby(by=["array"], group_keys=False) \
    .apply(calc_straight_grade_elevation)
output["sge_offset"] = output.apply(calc_sge_offset, axis=1)
output = output.groupby(by=["array"], group_keys=False) \
    .apply(straight_grade_check)

output = output.groupby(by=["array"], group_keys=False) \
    .apply(calc_best_fit_elevation)
output["bfe_offset"] = output.apply(calc_best_fit_offset, axis=1)
output = output.groupby(by=["array"], group_keys=False) \
    .apply(best_fit_check)

output["needs_grading"] = output["sge_check"] | output["bfe_check"]

output.to_csv(OUT_FILENAME, index=False)

output.sge_check.value_counts()
output.bfe_check.value_counts()

total_piles_passed = output["needs_grading"].sum()
total_piles = output.shape[0]

print("piles passed: {}".format(total_piles_passed))
print("total piles: {}".format(total_piles))
print("% piles passed: {}".format(total_piles_passed / total_piles))

check_by_array = output[["array", "needs_grading"]].drop_duplicates()

total_arrays_passed = check_by_array["needs_grading"].sum()
total_arrays = check_by_array.shape[0]

print("arrays passed: {}".format(total_arrays_passed))
print("total arrays: {}".format(total_arrays))
print("% arrays passed: {}".format(total_arrays_passed / total_arrays))
